<?php

defined('APP_PATH') || define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']));
defined('BASE_URL') || define('BASE_URL', str_replace($_SERVER['DOCUMENT_ROOT'], '', APP_PATH));

define('THEME_NAME', 'theme_name');

function get_header(){
	require APP_PATH.'/frontend/views/header.php';
}

function get_footer(){
	require APP_PATH.'/frontend/views/footer.php';
}

function __($text, $domain = 'default') {
	echo $text;
}

function body_class($class = ''){
	global $bodyClass;
	echo 'class="'.$bodyClass.'"';
}

function get_bloginfo($show = '', $filter = 'raw'){
	return '';
}

function language_attributes(){

}

function wp_head(){

}

function bloginfo($show=''){
	if($show === 'charset'){
		echo 'UTF-8';
	}
}

function home_url($path = '', $scheme = null){
	echo BASE_URL.$path;
}

function wp_nav_menu($args = array()){
	if($args['theme_location'] === 'primary_menu'){
		echo '<div class="site-primary-menu">
				<ul class="menu">
					<li class="menu-item">
						<a href="#">Home</a>
					</li>
				</ul>
			</div>';
	}
}

function do_action($tag, $arg = ''){
	if($tag === 'wpml_add_language_selector'){
		echo '';
	}
}

function get_template_directory_uri(){
	$url = dirname(BASE_URL);
	if(in_array($url, array('/', '\\'))){
		return '';
	}
	return $url;
}

function get_template_directory(){
	return APP_PATH;
}

function wp_footer(){

}

function wp_title(){

}
