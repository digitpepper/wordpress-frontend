<?php

defined('DIR') || define('DIR', basename(dirname(dirname(dirname(__FILE__)))));

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<?php
	/**
	 * <title> will be added via add_theme_support( 'title-tag' );
	 * @link http://codex.wordpress.org/Title_Tag
	 */
	?>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/'.DIR ?>/frontend/bower_components/normalize-css/normalize.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/'.DIR ?>/frontend/css/main.css?<?php echo @filemtime(get_template_directory().'/frontend/css/main.css') ?>">

	<script src="<?php echo get_template_directory_uri().'/'.DIR ?>/frontend/bower_components/jquery/dist/jquery.min.js"></script>

	<?php wp_head(); ?>

</head>
<body <?php body_class() ?>>
<div class="site-wrapper">
	<header class="site-header">
		<a href="<?php echo home_url('/') ?>" class="site-logo">
			<?php bloginfo('name') ?>
		</a>
		<div class="site-navigation">

			<?php wp_nav_menu( array( 'theme_location' => 'primary_menu', 'container_class' => 'site-primary-menu' ) ); ?>

		</div>

	</header>
